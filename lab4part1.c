#include <avr/io.h>
#include <util/delay.h>
#include <stdint.h> 
#include <avr/interrupt.h>

void timer0_init()
{
  TCCR0A|=(1<<WGM00);
  TCCR0B|=(1<<WGM02)|(1<<CS02);
  OCR0A=1;
  TCNT0=0;
  TIMSK0|=(1<<OCIE0A);
  sei();
}
volatile unsigned char top8_value;
ISR(ADC_vect){
	top8_value = ADCH;
}

ISR(TIMER0_COMPA_vect)
{
  PORTD |= (1 << 5);
}

int main(void)
{
  DDRD|=(1<<5)|(1<<6);
  ADMUX=0b01100000;  
  ADCSRA=0b10000111; 
  while(1){
    ADCSRA|=(1<<ADSC);
    while(ADCSRA&(1<<ADSC)){;}
    PORTD=ADCH;
  }
  return 0;
  
  
  
  
}