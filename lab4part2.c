#include <avr/io.h>
#include <util/delay.h>
#include <stdint.h> 
#include <avr/interrupt.h>

void timer0_init()
{
  TCCR0A|=(1<<WGM00);
  TCCR0B|=(1<<WGM02)|(1<<CS02);
  OCR0A=1;
  TCNT0=0;
  TIMSK0|=(1<<OCIE0A);
  sei();
}
volatile unsigned char top8_value;
ISR(ADC_vect){
	top8_value = ADCH;
}



int main(void)
{
Serial.begin(9600);
  DDRD |= (1 << 6)|(1 << 5);
  ADMUX = 0b01100011; 
  ADCSRA = 0b10001110; 
  sei(); 
  while(1){
    ADCSRA |= (1 << ADSC);
    PORTD = top8_value;
    Serial.println(top8_value);
  }
  return 0;
  
  
  
  
}